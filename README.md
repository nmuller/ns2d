# Two-dimensional Navier-Stokes solver in Julia

Incompressible two-dimensional Navier-Stokes solver using a pseudospectral method with periodic boundary conditions. Useful to study two-dimensional turbulence, inverse energy cascade and direct enstrophy cascade.

The main script is `ns2d_streamfunction.jl`. It solves the NS equations for the stream function:

$\partial_t \omega + J(\omega,\psi) = \nu \nabla^2 \omega - \alpha \omega + f$

where $J(\omega, \psi) = \partial_x \omega \partial_y \psi - \partial_y \omega \partial_x \psi$ is the Poisson bracket. 


# Run the code

To run the code, first initialize all its packages by running

`julia --project -e "using Pkg; Pkg.instantiate()"`

Then just modify the parameters inside ns2d_streamfunction.jl and run

`julia --project ns2d_streamfunction.jl`