import numpy as np
import matplotlib.pyplot as plt

path = 'outs/'

def compute_vorticity(vx, vy):
    return np.gradient(vy, axis=0) - np.gradient(vx, axis=1)

fig, ax = plt.subplots(1,2,figsize=(8,5))

for i,idx in enumerate([0,9]):
    filename1 = path+"vx."+str(idx).zfill(3)+".txt"
    filename2 = path+"vy."+str(idx).zfill(3)+".txt"

    vx = np.loadtxt(filename1)
    vy = np.loadtxt(filename2)

    vorticity = compute_vorticity(vx, vy)
    ax[i].imshow(vorticity, cmap='coolwarm')

plt.show()
