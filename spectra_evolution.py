import matplotlib.pyplot as plt
import numpy as np
# import matplotlib.animation as animation
import sys
import scipy as sp
from numba import njit, prange

path = "./outs/"
nsta = 10
nend = 100
nfiles = nend-nsta + 1
nskip = 20
NP = 64

if len(sys.argv)>1:
    path0 = sys.argv[1]
if len(sys.argv)>2:
    nfiles = int(sys.argv[2])

def load_velocity(path,index):
    vx=np.loadtxt(path+"vx."+str(index).zfill(3)+".txt")
    vy=np.loadtxt(path+"vy."+str(index).zfill(3)+".txt")
    return vx, vy

def load_single_velocity(path,index, orientation):
    if orientation==0: 
        orientation_str="x"
    else:
        orientation_str = "y"
    v=np.loadtxt(path+f"v{orientation_str}."+str(index).zfill(3)+".txt")
    return v

@njit(parallel=True, fastmath=True)
def spectrum1d(vf,k,kmax,NP):
    Ek = np.zeros(kmax+1)
    for i in prange(NP):
        for j in prange(NP):
            ks = np.sqrt(np.abs(k[i]**2 + k[j]**2))
            iks = int(min(np.round(ks), kmax+1))
            Etmp = abs(vf[i,j])**2
            Ek[iks] += Etmp
    norm = NP**2 * NP**2  # One is for the sum, the other (I think) for the fft
    return .5 * Ek / norm

def compute_spectrum(path_fields, idx, NP):
    kmax = NP//3
    Ek = np.zeros(kmax+1)
    kf = np.fft.fftfreq(NP)*NP
    for i in range(2):
        vi = load_single_velocity(path_fields, idx, i)
        vif = sp.fft.fft2(vi, workers=-1)
        Ek_tmp = spectrum1d(vif, kf, kmax, NP)
        Ek += Ek_tmp
    return Ek

Lx = 2*np.pi
dx = Lx/float(NP)
kmax = NP//3
k = np.arange(1, kmax+1)

cmap = plt.cm.viridis
colors = [cmap(i/nend) for i in range(nend+1)]
for i in range(nsta, nend+1, nskip):
    Ek = compute_spectrum(path, i, NP)
    plt.loglog(k[:-2], Ek[1:-2], label=i, color=colors[i])

plt.loglog(k, (k/k[2])**(-5/3.)*Ek[3], 'k--')
plt.loglog(k, (k/k[2])**(-3.)*Ek[3], 'r--')
plt.loglog(k, (k/k[2])**(-4.)*Ek[3], 'r--')
plt.legend(ncol=2)

plt.show()
