import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import sys
from numba import njit, prange

path = "./outs/"
nfiles = 100

if len(sys.argv)>1:
    path0 = sys.argv[1]
if len(sys.argv)>2:
    nfiles = int(sys.argv[2])

def load_velocity(path,index):
  vx=np.loadtxt(path+"vx."+str(index).zfill(3)+".txt")
  vy=np.loadtxt(path+"vy."+str(index).zfill(3)+".txt")
  return vx, vy

@njit(parallel=True, fastmath=True)
def compute_energy_1d(v):
    Nx, Ny = v.shape
    energy = 0
    for i in prange(Nx):
        for j in prange(Ny):
            energy = energy + v[i,j]**2
    return energy
    
energy = np.zeros(nfiles+1)
for i in range(nfiles+1):
    vx, vy = load_velocity(path,i) 
    energy[i] = .5*np.mean(vx**2 + vy**2)
    # energy[i] = compute_energy_1d(vx)
    # energy[i] += compute_energy_1d(vy)
    # energy[i] *= 0.5

NPx, NPy = vx.shape
# energy /= (NPx*NPy)
fig, ax = plt.subplots(figsize=(8,5))

ax.plot(energy)

plt.show()
