using FFTW
using LinearAlgebra
using TimerOutputs
using Statistics
using DelimitedFiles
using Random
using ProgressMeter

mutable struct NavierStokes{T}
    ψ::Array{Complex{T},2}    # stream function
    ω::Array{Complex{T},2}    # vorticity
    J::Array{Complex{T},2}    # Poisson brackets
    ν::Float64                # viscosity
    α::Float64                # linear damping
    f::Array{Complex{T},2}    # external forcing
    fx::Array{Complex{T},2}   # Fourier transform of the x-component of the forcing
    fy::Array{Complex{T},2}   # Fourier transform of the y-component of the forcing
    k1::Array{Complex{T},2}   # RHS in Euler
    k2::Array{Complex{T},2}   # Second field for RK2
    k3::Array{Complex{T},2}   # Third  field for RK4
    k4::Array{Complex{T},2}   # Fourth field for RK4
    ψint::Array{Complex{T},2} # stream function for intermediate steps
end

function rhs!(NS::NavierStokes{T}; step=1, to=TimerOutput()) where {T}
    """ 
    Routine to compute the right hand side of the 2D incompressible Navier-Stokes equations for
    periodic boundary conditions using a pseudospectral method and the 2/3 rule for dealiasing.

    Inputs:
    NS: NavierStokes structure
    step: Determines which field of NS is modified (for different RK steps)
    to: Timer Output
    """

    @timeit to "vorticity" vorticity_from_streamfunction!(NS)
    @timeit to "poisson" poisson!(NS)

    if step==1
        @timeit to "forcing" random_forcing!(NS, kmin_forcing, kmax_forcing, Amp_forcing)
    end
    # @timeit to "forcing" gaussian_forcing!(f, kcenter, kwidth, Amp_forcing)

    # Compute the right hand side of the Navier-Stokes equations
    @timeit to "rhs" begin
        NS.fx .= NS.J ./ (kk2.+1e-10) # Non-linear term
        NS.fy .= -ν .* NS.ω          # Dissipative term
        if step==1
            NS.k1 .= NS.fy .- NS.fx .- NS.α .* NS.ψ .+ dt.*NS.f 
        elseif step==2
            NS.k2 .= NS.fy .- NS.fx .- NS.α .* NS.ψ .+ dt.*NS.f 
        elseif step==3
            NS.k3 .= NS.fy .- NS.fx .- NS.α .* NS.ψ .+ dt.*NS.f 
        elseif step==4
            NS.k4 .= NS.fy .- NS.fx .- NS.α .* NS.ψ .+ dt.*NS.f 
        end
    end

    nothing
end

function laplacian(u)
    """
    Compute the laplacian.
    Inputs:
    u: Fourier transform of the velocity field
    """

    lap_field = -k2.*u

    return lap_field
end

function vorticity_from_streamfunction!(NS::NavierStokes{T}) where {T}
    NS.ω .= kk2 .* NS.ψ
    nothing
end

function velocity_from_streamfunction(ψ)
    u = 1im .* ky .* ψ
    v =-1im .* kx .* ψ
    return u,v
end

function poisson!(NS::NavierStokes{T}) where {T}
    """
    Computes de poisson bracket in real space in space:
    [ω,ψ] = ω_x ψ_y - ω_y ψ_x
    And saves it in J. It uses fx and fy as intermediate fields.
    """

    # Computes the first term of the Poisson bracket in real space
    NS.fx .= 1im .* kx .* NS.ω # ω_x
    plan_cr!*NS.fx
    NS.fy .= 1im .* ky .* NS.ψ # ψ_y
    plan_cr!*NS.fy
    NS.J .= NS.fx .* NS.fy 

    # Computes the second term of the Poisson bracket in real space
    NS.fy .= 1im .* ky .* NS.ω # ω_y
    plan_cr!*NS.fy
    NS.fx .= 1im .* kx .* NS.ψ # ψ_y
    plan_cr!*NS.fx
    NS.J .-= NS.fy .* NS.fx

    # Returns the poisson Bracket in Fourier space after dealiasing
    plan_rc!*NS.J
    dealias!(NS.J)

    nothing
end

function rotor!(out, u, v)
    """
    Compute the rotational.
    Inputs:
    u: Fourier transform of the velocity field in x
    v: Fourier transform of the velocity field in y
    Output:
    out: rotational of u and v
    """
    out .= 1im.*kx.*v .- 1im.*ky.*u
    nothing
end

function rotor!(NS::NavierStokes{T}) where {T}
    """
    Compute the rotational.
    Inputs:
    u: Fourier transform of the velocity field in x
    v: Fourier transform of the velocity field in y
    Output:
    out: rotational of u and v
    """
    NS.f .= 1im.*kx.*NS.fx .- 1im.*ky.*NS.fy
    nothing
end

function meshgrid(xin,yin)
    """
    Generates a two-dimensional grid from two 1D arrays.
    """
    nx = length(xin)
    ny = length(yin)
    xout = ones(nx)' .* xin
    yout = yin' .* ones(ny)
    return (x=xout, y=yout)
end

function fftfreq(n, d)
    k = (0:n÷2-1)
    k = [k; -n÷2:-1]
    return 2π*k./(n*d)
end

function dealias!(field)
    """
    In place dealiasing of the input field following the Orszag 2/3 rule. 
    """
    field .*= Kill
end

function init_spectral(nx, ny, dx, dy)
    """
    Initialization of the wave numbers kx, ky and k2=k^2 and the dealiasing matrix Kill.
    """
    kmax = max(nx, ny) ÷ 3 - 1
    Kill = ones(nx, ny)
    kx = fftfreq(nx, dx)
    ky = fftfreq(ny, dy)
    @inbounds for j=1:ny
        for i=1:nx
            k = sqrt.(kx[i].^2 + ky[j].^2)
            if k>=kmax
                Kill[i,j] = 0
            end
        end
    end
    kx, ky = meshgrid(kx, ky)
    kk2 = kx.^2 .+ ky.^2
    kk = sqrt.(kk2)
    return kx, ky, kk, kk2, Kill
end

function init_fft(u)
    """
    Initialization of the fft plans for direct and inverse transforms.
    """
    plan_rc = plan_fft(u;  flags=FFTW.MEASURE, timelimit=Inf)
    plan_cr = plan_ifft(u; flags=FFTW.MEASURE, timelimit=Inf)
    plan_rc! = plan_fft!(u;  flags=FFTW.MEASURE, timelimit=Inf)
    plan_cr! = plan_ifft!(u; flags=FFTW.MEASURE, timelimit=Inf)
    return plan_rc, plan_cr, plan_rc!, plan_cr!
end

function random_forcing!(NS::NavierStokes{T}, kmin, kmax, Amp_forcing) where {T}
    """ 
    Generates a random forcing field in Fourier space for the 2D Navier-Stokes equations. It
    takes in the following input arguments:

        fx: a 2D array representing the x-component of the forcing field.
        fy: a 2D array representing the y-component of the forcing field.
        kmin: the minimum wavenumber for the forcing.
        kmax: the maximum wavenumber for the forcing.
        Amp_forcing: the amplitude of the forcing.
    """

    NS.fx .= 0.0 + 0.0*im
    NS.fy .= 0.0 + 0.0*im
    krange = (kmin^2 .<= kk2 .<= kmax^2)
    phases = (2π) .* rand(sum(krange))
    norms = 1 ./ kk[krange]
    NS.fx[krange] .= norms .* (cos.(phases) .+ 1im .* sin.(phases))
    phases .= (2π) .* rand(sum(krange))
    NS.fy[krange] .= norms .* (cos.(phases) .+ 1im .* sin.(phases))
    # energy = 0.5*mean(abs.(fx).^2 .+ abs.(fy).^2)
    # fx .*= Amp_forcing / energy
    # fy .*= Amp_forcing / energy

    # Take rotor to make it incompressible
    rotor!(NS)
    energy = 0.5*mean(abs.(NS.f).^2) 
    NS.f .*= (Amp_forcing / energy)

    nothing
end

function gaussian_forcing!(f, kcenter, width, Amp_forcing)
    """ 
    Generates a gaussian forcing field in Fourier space for the 2D Navier-Stokes equations. It
    takes in the following input arguments:

        fx: a 2D array representing the x-component of the forcing field.
        fy: a 2D array representing the y-component of the forcing field.
        kcenter: wave number where the gaussian is centered.
        width: width of the gaussian.
        Amp_forcing: the amplitude of the forcing.
    """
    C = Amp_forcing/dx/dy / (2π*width^2)
    # C = Amp_forcing #/ (2π*width^2)
    # krange = (kmin^2 .<= kk2 .<= kmax^2)
    phases = (2π) .* rand(nx,ny)
    @inbounds for j=1:ny
        # kyj = ky[j]
        @inbounds for i=1:nx
            k = kk[i,j]
            phase = phases[i,j]
            if abs(k - kcenter) < 2*width
                f[i,j] = C*exp(-0.5*((k - kcenter)/width)^2) * (cos(phase) + 1im*sin(phase))
            else
                f[i,j] = 0.0 + 0.0im
            end
        end
    end
end

function save_fields(ψ, index)
    """
    Save velocity fields obtained from stream function in files of the type
        vx.053.txt
        vy.100.txt
        psi.000.txt
    """
    output_dir = "outs/"
    filename1 = output_dir*"vx."*lpad(index,3,"0")*".txt"
    filename2 = output_dir*"vy."*lpad(index,3,"0")*".txt"
    filename3 = output_dir*"psi."*lpad(index,3,"0")*".txt"
    u, v = velocity_from_streamfunction(ψ)
    u_out = real.(plan_cr*u)
    v_out = real.(plan_cr*v)
    ψ_out = real.(plan_cr*ψ)
    writedlm(filename1, u_out)
    writedlm(filename2, v_out)
    writedlm(filename3, ψ_out)
    nothing
end

function read_streamfunction(path, index)
    filename = path*"psi."*lpad(index,3,"0")*".txt"
    ψ = readdlm(filename)
    return ψ
end

function solve_navier_stokes!(NS::NavierStokes{T}, dt, nsteps, to) where {T}
    """
    Main Navier-Stokes solver in two-dimensions using a pseudospectral method and periodic boundary conditions. It implements a Runge-Kutta method of order 4 for the time evolution.

    Inputs:
    NS: NavierStokes structure
    dt: Time step.
    nsteps: Number of steps in the evolution.
    to: Timer output.
    """

    # Save initial condition
    if restart==0
        @timeit to "save" save_fields(NS.ψ, restart)
    end

    # Define the time step function using Euler and modifies in place ψ
    function Euler!(NS::NavierStokes{T}, dt) where {T}
        """
        Time evolution using Euler.
        """

        # Compute the right-hand side of the Navier-Stokes equations in k1
        @timeit to "RHS" rhs!(NS; step=1, to)
        NS.ψ .+= dt*NS.k1

        nothing
    end

    # Define the time step function using RK2 and modifies in place ψ
    function RK2!(NS::NavierStokes{T}, dt) where {T}
        """
        Time evolution using RK2.
        """

        # Compute the right-hand side of the Navier-Stokes equations
        @timeit to "RHS" rhs!(NS; step=1, to)

        # Compute the intermediate solutions for RK4
        NS.ψint = NS.ψ .+ (dt).*NS.k1
        @timeit to "RHS" rhs!(NS; step=2, to)

        # Compute the final solution
        NS.ψ .+= (dt/2) .* (NS.k1 .+ NS.k2)

        nothing
    end

    # Define the time step function using RK4 and modifies in place ψ
    function RK4!(NS::NavierStokes{T}, dt) where {T}
        """
        Time evolution using RK4.
        """

        # Compute the right-hand side of the Navier-Stokes equations
        @timeit to "RHS" rhs!(NS; step=1, to)

        # Compute the intermediate solutions for RK4
        NS.ψint = NS.ψ .+ (0.5*dt).*NS.k1
        @timeit to "RHS" rhs!(NS; step=2, to)
        NS.ψint .= NS.ψ .+ (0.5*dt).*NS.k2
        @timeit to "RHS" rhs!(NS; step=3, to)
        NS.ψint .= NS.ψ .+ dt.*NS.k3
        @timeit to "RHS" rhs!(NS; step=4, to)

        # Compute the final solution
        NS.ψ .+= (dt/6) .* (NS.k1 .+ 2 .* NS.k2 .+ 2 .* NS.k3 .+ NS.k4)

        nothing
    end

    # Define the time loop
    save_idx = restart+1
    @showprogress "Simulation ..." for i=1:nsteps 
    # for i=1:nsteps 

        # Evolve ψ in time
        # @timeit to "Euler" Euler!(NS, dt)
        @timeit to "RK2" RK2!(NS, dt)
        # @timeit to "RK4" RK4!(NS, dt)

        if mod(i,nsave÷20)==0
            # Check if it is running well
            @timeit to "CFL" begin
                u, v = velocity_from_streamfunction(NS.ψ)
                u = real.(plan_cr*u)
                v = real.(plan_cr*v)
                Vmax = max(maximum(u), maximum(v))
                CFL = Vmax*dt/dx
                # println(CFL)
                if CFL >= .005
                    dt /= 1.5
                end
                if CFL <= .001
                    dt *= 1.5
                end
                # println(dt)
            end
            if sum(isnan.(NS.ψ)) > 0
                throw(error("Diverge in step $i"))
            end
        end

        if mod(i,nsave)==0
            @timeit to "save" save_fields(NS.ψ, save_idx)
            save_idx += 1
        end

    end

    nothing
end

####

# Main code starts here

# Parameters
nx = 64          # Number of grid points in x
ny = 64          # Number of grid points in y
Lx = 2π         # Length in x
Ly = 2π         # Length in y
ν  = 0.0005       # Viscosity
α  = 0.001       # Large scale dissipation
dt = 0.003        # Temporal time step
nsteps = 50000    # Total number of time steps for the evolution
nsave = 500       # Saving each nsave time steps
restart = 0

# Initial conditions
Amp = 1. # Ampltitud of perturbation
E = .5     # Energy of initial condition

# Forcing
Amp_forcing = 1.0  # Amplitud of the forcing
kmin_forcing = 8   # Minimum wave number of the forcing
kmax_forcing = 10  # Maximum wave number of the forcing
kcenter = 10
kwidth = 2

dx = Lx/nx       # Spatial resolution in x
dy = Ly/ny       # Spatial resolution in y
t = Array(0:dt:dt*nsteps) # Time

# Allocate arrays and structure
T = Float32
ψ = zeros(Complex{T}, nx, ny)
ω = zeros(Complex{T}, nx, ny)
J = zeros(Complex{T}, nx, ny)
f = zeros(Complex{T}, nx, ny)
fx= zeros(Complex{T}, nx, ny)
fy= zeros(Complex{T}, nx, ny)
k1= zeros(Complex{T}, nx, ny)
k2= zeros(Complex{T}, nx, ny)
k3= zeros(Complex{T}, nx, ny)
k4= zeros(Complex{T}, nx, ny)
ψint = zeros(Complex{T}, nx, ny)
NS = NavierStokes(ψ, ω, J, ν, α, f, fx, fy, k1, k2, k3, k4, ψint)
# Spectral quantities
# ψ = zeros(ComplexF64, nx, ny)
plan_rc, plan_cr, plan_rc!, plan_cr! = init_fft(ψ)
kx, ky, kk, kk2, Kill = init_spectral(nx, ny, dx, dy)

# Define grid for initial condition
x = range(0, Lx, length=nx+1)[1:nx]  # Grid points in x-direction
y = range(0, Ly, length=ny+1)[1:ny]  # Grid points in y-direction
XX, YY = meshgrid(x, y)


# Define external forcing
# random_forcing!(f, fx, fy, kmin_forcing, kmax_forcing, Amp)
# gaussian_forcing!(f, kcenter, kwidth, Amp_forcing)

# Define the initial condition in real space
if restart == 0
    # ψ0 =  sin.(1 .* XX) .* sin.(2 .* YY) .+ randn(Random.seed!(123), nx,ny).*(Amp*dx)
    # ψ0 =  sin.(XX) .+ randn(Random.seed!(123), nx,ny).*(Amp*dx)
    # ψ0 = zeros(nx, ny)
    random_forcing!(NS, kmin_forcing, kmax_forcing, Amp)
    ψ = NS.f
    ψ0 = real(plan_cr*ψ)

    # Compute velocity field in real space
    ψ0f = plan_rc*ψ0
    u0f, v0f = velocity_from_streamfunction(ψ0f)
    u0 = real(plan_cr*u0f)
    v0 = real(plan_cr*v0f)

    # Normalize energy of initial condition
    energy = 0.5*mean(u0.^2 .+ v0.^2) 
    println(energy)
    if energy!=0
        ψ0 .*= E/energy
    end

else
    ψ0 = read_streamfunction("outs/", restart)
end

f .= 0.0 + 0.0*im
# gaussian_forcing!(f, kcenter, kwidth, Amp_forcing)

# Solve
NS.ψ .= plan_rc*ψ0
to = TimerOutput()
@timeit to "main" solve_navier_stokes!(NS, dt, nsteps, to) 

# ψ = NS.ψ
# ω = similar(ψ)
# uf, vf = velocity_from_streamfunction(ψ)
# rotor!(ω, uf, vf)
# u = real.(plan_cr*uf)
# v = real.(plan_cr*vf)

print_timer(to)

