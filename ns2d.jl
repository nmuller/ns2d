using FFTW
using LinearAlgebra
using TimerOutputs
using Statistics
using DelimitedFiles
using Random
# using PyPlot
# using CSV
# const plt = PyPlot.plt

function navier_stokes_rhs(u::Array{T,2}, v::Array{T,2}, nu, fx, fy, to) where {T}
    """ 
    Routine to compute the right hand side of the 2D incompressible Navier-Stokes equations for
    periodic boundary conditions using a pseudospectral method and the 2/3 rule for dealiasing.

    Inputs:
    u: fourier transform of the velocity field vx
    v: fourier transform of the velocity field vy
    nu: viscosity
    f: fourier transform of the external forcing
    to: Timer Output

    Outputs:
    rhs_u: right hand side of vx in spectral space
    rhs_v: right hand side of vy in spectral space
    """

    @timeit to "forcing" random_forcing!(fx, fy, kmin_forcing, kmax_forcing, Amp_forcing)
    # @timeit to "forcing" gaussian_forcing!(fx, fy, kcenter, kwidth, Amp_forcing)

    # Compute the Laplacian of u and v
    rhs_u = @timeit to "laplacian" laplacian(u)
    rhs_v = @timeit to "laplacian" laplacian(v)

    # Compute the convection term of u and v
    conv_u, conv_v = @timeit to "convection" convection(u, v)
    # Incompressible projection due to the pressure gradient
    @timeit to "projection" incompressible_projection!(conv_u, conv_v)

    # Compute the right hand side of the Navier-Stokes equations
    rhs_u .= @timeit to "rhs" nu.*rhs_u .- conv_u .+ fx .- alpha.*u
    rhs_v .= @timeit to "rhs" nu.*rhs_v .- conv_v .+ fy .- alpha.*v

    return rhs_u, rhs_v
end

# Compute the Laplacian of a field using a pseudo-spectral method
function laplacian(u)
    """
    Compute the laplacian.
    Inputs:
    u: Fourier transform of the velocity field
    """

    lap_field = -k2.*u

    return lap_field
end

# Compute the convection term of a field using a pseudo-spectral method
function convection(u, v)
    """ 
    Computes the convective term of the Navier-Stokes equation w.grad(w) in real space. Performs a
    dealiasing following the 2/3 rule.
    Inputs:
    u: Fourier transform of the velocity field vx
    v: Fourier transform of the velocity field vy
    """

    # Compute Fourier transforms of u and v
    u_real = plan_cr * u
    v_real = plan_cr * v

    # Compute derivative of u and v
    du_dx = plan_cr * (1im .* kx .* u)
    du_dy = plan_cr * (1im .* ky .* u)
    dv_dx = plan_cr * (1im .* kx .* v)
    dv_dy = plan_cr * (1im .* ky .* v)

    # This doesn't work I don't know why
    # plan_cr!*u
    # plan_cr!*v

    # Compute convective terms in real space
    conv_u = u_real .* du_dx .+ v_real .* du_dy
    conv_v = u_real .* dv_dx .+ v_real .* dv_dy
    # conv_u = u .* du_dx .+ v .* du_dy
    # conv_v = u .* dv_dx .+ v .* dv_dy

    # Output in fourier space
    plan_rc!*conv_u
    plan_rc!*conv_v

    # Dealias output
    dealias!(conv_u)
    dealias!(conv_v)

    return conv_u, conv_v
end

# Incompressible projection for the pressure gradient term
function incompressible_projection!(u, v)
    """ 
    Computes the incompressible projection due to the pressure gradient in spectral space defined as
    Pk = (1 - k_i k_j/k^2)conv_j with conv_j the nonlinear term: conv_x = u ; conv_y = v. 
    Inputs:
    u: x component of the nonlinear term in Fourier space
    v: y component of the nonlinear term in Fourier space
    """

    norm = 1 ./ (k2 .+ 1e-10)
    kv = (kx.*u .+ ky.*v) .* norm
    u .-= kx.*kv 
    v .-= ky.*kv 

end

function meshgrid(xin,yin)
    nx = length(xin)
    ny = length(yin)
    xout = ones(nx)' .* xin
    yout = yin' .* ones(ny)
    return (x=xout, y=yout)
end

# Define the function to calculate the vorticity
function vorticity(u, v)
    u_hat = plan_rc*u
    v_hat = plan_rc*v
    return real.(plan_cr*(1im.*kx.*v_hat .- 1im.*ky.*u_hat))
end

# Compute the frequency grid
function fftfreq(n, d)
    k = (0:n÷2-1)
    k = [k; -n÷2:-1]
    return 2π*k./(n*d)
end

function dealias!(field)
    """
    In place dealiasing of the input field following the Orszag 2/3 rule. 
    """
    # field .*= Kill
    field .= Kill.*field
end

function init_spectral(nx, ny, dx, dy)
    """
    Initialization of the wave numbers kx, ky and k2=k^2 and the dealiasing matrix Kill.
    """
    kmax = max(nx, ny) ÷ 3
    Kill = ones(nx, ny)
    kx = fftfreq(nx, dx)
    ky = fftfreq(ny, dy)
    @inbounds for j=1:ny
        for i=1:nx
            k = sqrt.(kx[i].^2 + ky[j].^2)
            if k>kmax
                Kill[i,j] = 0
            end
        end
    end
    kx, ky = meshgrid(kx, ky)
    k2 = kx.^2 .+ ky.^2
    kk = sqrt.(k2)
    return kx, ky, kk, k2, Kill
end

function init_fft(u)
    """
    Initialization of the fft plans for direct and inverse transforms.
    """
    plan_rc = plan_fft(u;  flags=FFTW.MEASURE, timelimit=Inf)
    plan_cr = plan_ifft(u; flags=FFTW.MEASURE, timelimit=Inf)
    plan_rc! = plan_fft!(u;  flags=FFTW.MEASURE, timelimit=Inf)
    plan_cr! = plan_ifft!(u; flags=FFTW.MEASURE, timelimit=Inf)
    return plan_rc, plan_cr, plan_rc!, plan_cr!
end

function random_forcing!(fx, fy, kmin, kmax, Amp_forcing)
    """ 
    Generates a random forcing field in Fourier space for the 2D Navier-Stokes equations. It
    takes in the following input arguments:

        fx: a 2D array representing the x-component of the forcing field.
        fy: a 2D array representing the y-component of the forcing field.
        kmin: the minimum wavenumber for the forcing.
        kmax: the maximum wavenumber for the forcing.
        Amp_forcing: the amplitude of the forcing.
    """

    krange = (kmin^2 .<= k2 .<= kmax^2)
    phases = (2π) .* rand(sum(krange))
    norms = 1 ./ kk[krange]
    fx[krange] .= norms .* (cos.(phases) .+ 1im .* sin.(phases))
    fy[krange] .= norms .* (cos.(phases) .+ 1im .* sin.(phases))
    energy = 0.5*mean(abs.(fx).^2 .+ abs.(fy).^2)
    fx .*= Amp_forcing / energy
    fy .*= Amp_forcing / energy

    incompressible_projection!(fx, fy)
    dealias!(fx)
    dealias!(fy)

    nothing
end

function gaussian_forcing!(fx, fy, kcenter, width, Amp_forcing)
    """ 
    Generates a gaussian forcing field in Fourier space for the 2D Navier-Stokes equations. It
    takes in the following input arguments:

        fx: a 2D array representing the x-component of the forcing field.
        fy: a 2D array representing the y-component of the forcing field.
        kcenter: wave number where the gaussian is centered.
        width: width of the gaussian.
        Amp_forcing: the amplitude of the forcing.
    """
    C = Amp_forcing / (2π*width^2)
    @inbounds for j=1:ny
        kyj = ky[j]
        @inbounds for i=1:nx
            k = sqrt(kx[i]^2 + kyj^2)
            if k > 0
                #norm = 1/k
                phase = 2π*rand()
                if abs(k - kcenter) < 2*width
                    fx[i,j] = C*exp(-0.5*((k - kcenter)/width)^2) * (cos(phase) + 1im*sin(phase))
                    fy[i,j] = C*exp(-0.5*((k - kcenter)/width)^2) * (cos(phase) + 1im*sin(phase))
                else
                    fx[i,j] = 0.0 + 0.0im
                    fy[i,j] = 0.0 + 0.0im
                end
            else
                fx[i,j] = 0.0 + 0.0im
                fy[i,j] = 0.0 + 0.0im
            end
        end
    end
end

function save_fields(u, v, index)
    output_dir = "outs/"
    filename1 = output_dir*"vx."*lpad(index,3,"0")*".txt"
    filename2 = output_dir*"vy."*lpad(index,3,"0")*".txt"
    u_out = real.(plan_cr*u)
    v_out = real.(plan_cr*v)
    writedlm(filename1, u_out)
    writedlm(filename2, v_out)
    nothing
end

function solve_navier_stokes(u0::Array{T,2}, v0::Array{T,2}, nu, dt, nsteps, fx, fy, to) where {T}
    """
    Main Navier-Stokes solver in two-dimensions using a pseudospectral method and periodic boundary conditions. It implements a Runge-Kutta method of order 4 for the time evolution.

    Inputs:
    u0: Initial velocity field vx in real space.
    v0: Initial velocity field vy in real space.
    nu: Viscosity.
    dt: Time step.
    nsteps: Number of steps in the evolution.
    f: External forcing in Fourier space.
    to: Timer output.

    Outputs:
    u: vx in Fourier space.
    v: vy in Fourier space.
    """


    # Define the initial condition

    u = plan_rc*u0
    v = plan_rc*v0

    # Save initial condition
    @timeit to "save" save_fields(u, v, 0)

    # Define the time step function
    function step_function!(u, v, nu, dt, fx, fy)
        """
        Time evolution using RK4.
        """

        # Compute the right-hand side of the Navier-Stokes equations
        ku1, kv1 = @timeit to "RHS" navier_stokes_rhs(u, v, nu, fx, fy, to)

        # Compute the intermediate solutions for RK4
        uint = u .+ (0.5*dt).*ku1
        vint = v .+ (0.5*dt).*kv1
        ku2, kv2 = @timeit to "RHS" navier_stokes_rhs(uint, vint, nu, fx, fy, to)
        uint .= u .+ (0.5*dt).*ku2
        vint .= v .+ (0.5*dt).*kv2
        ku3, kv3 = @timeit to "RHS" navier_stokes_rhs(uint, vint, nu, fx, fy, to)
        uint .= u .+ dt.*ku3
        vint .= v .+ dt.*kv3
        ku4, kv4 = @timeit to "RHS" navier_stokes_rhs(uint, vint, nu, fx, fy, to)

        # Compute the final solution
        u .+= (dt/6) .* (ku1 .+ 2 .* ku2 .+ 2 .* ku3 .+ ku4)
        v .+= (dt/6) .* (kv1 .+ 2 .* kv2 .+ 2 .* kv3 .+ kv4)

        # Just Euler
        # u .+= dt.*ku1 
        # v .+= dt.*kv1 

    end

    # Define the time loop
    save_idx = 1
    for i=1:nsteps 
        # Update the solution
        @timeit to "time step" step_function!(u, v, nu, dt, fx, fy)

        if mod(i,nsave)==0
            @timeit to "save" save_fields(u, v, save_idx)
            save_idx += 1
        end

    end

    return u, v
end

####

# Main code starts here

# Parameters
nx = 32          # Number of grid points in x
ny = 32          # Number of grid points in y
Lx = 2pi         # Length in x
Ly = 2pi         # Length in y
nu = 0.02        # Viscosity
alpha = 0.       # Large scale dissipation
dt = 0.01        # Temporal time step
nsteps = 5000    # Total number of time steps for the evolution
nsave = 50       # Saving each nsave time steps

# Initial conditions
Amp = 0.2 # Ampltitud of perturbation
E = 1     # Energy of initial condition

# Forcing
Amp_forcing = 0.0  # Amplitud of the forcing
kmin_forcing = 1  # Minimum wave number of the forcing
kmax_forcing = 5  # Maximum wave number of the forcing
kcenter = 10
kwidth = 5

dx = Lx/nx       # Spatial resolution in x
dy = Ly/ny       # Spatial resolution in y
t = Array(0:dt:dt*nsteps) # Time

# Define grid for initial condition
x = range(0, Lx, length=nx+1)[1:nx]  # Grid points in x-direction
y = range(0, Ly, length=ny+1)[1:ny]  # Grid points in y-direction
XX, YY = meshgrid(x, y)

# Define the initial condition
u0 =  2 .* sin.(1 .* XX) .* cos.(2 .* YY) .+ randn(Random.seed!(123), nx,ny).*Amp
v0 = -cos.(1 .* XX) .* sin.(2 .* YY) .+ randn(Random.seed!(123), nx,ny).*Amp
# u0 = zeros(nx, ny)
# v0 = zeros(nx, ny)

# Normalize energy of initial condition
energy = 0.5*mean(abs.(u0).^2 .+ abs.(v0).^2)
println(energy)
if energy!=0
    u0 .*= E/energy
    v0 .*= E/energy
end

# Spectral quantities
u = zeros(ComplexF64, nx, ny)
plan_rc, plan_cr, plan_rc!, plan_cr! = init_fft(u)
kx, ky, kk, k2, Kill = init_spectral(nx, ny, dx, dy)

# Define external forcing
fx = zeros(ComplexF64, nx, ny)
fy = zeros(ComplexF64, nx, ny)
random_forcing!(fx, fy, kmin_forcing, kmax_forcing, Amp_forcing)
# gaussian_forcing!(fx, fy, kcenter, kwidth, Amp_forcing)

# Solve
to = TimerOutput()
u, v = @timeit to "main" solve_navier_stokes(u0, v0, nu, dt, nsteps, fx, fy, to) 

print_timer(to)
