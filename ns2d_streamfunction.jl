using FFTW
using LinearAlgebra
using TimerOutputs
using Statistics
using DelimitedFiles
using Random
using ProgressMeter

function navier_stokes_rhs!(rhs::Array{T,2}, ψ::Array{T,2}, ω::Array{T,2}, J::Array{T,2}, ν, 
    f::Array{T,2}, fx::Array{T,2}, fy::Array{T,2}; to=TimerOutput()) where {T}
    """ 
    Routine to compute the right hand side of the 2D incompressible Navier-Stokes equations for
    periodic boundary conditions using a pseudospectral method and the 2/3 rule for dealiasing.

    Inputs:
    ψ: fourier transform of the stream function 
    ν: viscosity
    f: fourier transform of the external forcing
    to: Timer Output

    Outputs:
    rhs: right hand side of the NS equation in spectral space
    """

    @timeit to "vorticity" vorticity_from_streamfunction!(ω, ψ)
    @timeit to "poisson" poisson!(J, ω, ψ, fx, fy)

    @timeit to "forcing" random_forcing!(f, fx, fy, kmin_forcing, kmax_forcing, Amp_forcing)
    # @timeit to "forcing" gaussian_forcing!(f, kcenter, kwidth, Amp_forcing)

    # Compute the right hand side of the Navier-Stokes equations
    @timeit to "rhs" begin
        fx .= J ./ (k2.+1e-10) # Non-linear term
        fy .= -ν .* ω          # Dissipative term
        rhs .= fy .- fx .- α.*ψ .+ f 
    end

    nothing
end

function laplacian(u)
    """
    Compute the laplacian.
    Inputs:
    u: Fourier transform of the velocity field
    """

    lap_field = -k2.*u

    return lap_field
end

function vorticity_from_streamfunction!(ω, ψ)
    ω .= k2 .* ψ
    nothing
end

function velocity_from_streamfunction(ψ)
    u = 1im .* ky .* ψ
    v =-1im .* kx .* ψ
    return u,v
end

function poisson!(J, ω, ψ, fx, fy)
    """
    Computes de poisson bracket in real space in space:
    [ω,ψ] = ω_x ψ_y - ω_y ψ_x
    And saves it in J. It uses fx and fy as intermediate fields.
    """

    # Computes the first term of the Poisson bracket in real space
    fx .= 1im .* kx .* ω
    plan_cr!*fx
    fy .= 1im .* ky .* ψ
    plan_cr!*fy
    J .= fx .* fy 

    # Computes the second term of the Poisson bracket in real space
    fy .= 1im .* ky .* ω
    plan_cr!*fy
    fx .= 1im .* kx .* ψ
    plan_cr!*fx
    J .-= fy .* fx

    # Returns the poisson Bracket in Fourier space after dealiasing
    plan_rc!*J
    dealias!(J)

    nothing
end

function rotor!(out, u, v)
    """
    Compute the rotational.
    Inputs:
    u: Fourier transform of the velocity field in x
    v: Fourier transform of the velocity field in y
    Output:
    out: rotational of u and v
    """
    out .= 1im.*kx.*v .- 1im.*ky.*u
    nothing
end

function meshgrid(xin,yin)
    """
    Generates a two-dimensional grid from two 1D arrays.
    """
    nx = length(xin)
    ny = length(yin)
    xout = ones(nx)' .* xin
    yout = yin' .* ones(ny)
    return (x=xout, y=yout)
end

function fftfreq(n, d)
    k = (0:n÷2-1)
    k = [k; -n÷2:-1]
    return 2π*k./(n*d)
end

function dealias!(field)
    """
    In place dealiasing of the input field following the Orszag 2/3 rule. 
    """
    field .*= Kill
end

function init_spectral(nx, ny, dx, dy)
    """
    Initialization of the wave numbers kx, ky and k2=k^2 and the dealiasing matrix Kill.
    """
    kmax = max(nx, ny) ÷ 3 - 1
    Kill = ones(nx, ny)
    kx = fftfreq(nx, dx)
    ky = fftfreq(ny, dy)
    @inbounds for j=1:ny
        for i=1:nx
            k = sqrt.(kx[i].^2 + ky[j].^2)
            if k>=kmax
                Kill[i,j] = 0
            end
        end
    end
    kx, ky = meshgrid(kx, ky)
    k2 = kx.^2 .+ ky.^2
    kk = sqrt.(k2)
    return kx, ky, kk, k2, Kill
end

function init_fft(u)
    """
    Initialization of the fft plans for direct and inverse transforms.
    """
    plan_rc = plan_fft(u;  flags=FFTW.MEASURE, timelimit=Inf)
    plan_cr = plan_ifft(u; flags=FFTW.MEASURE, timelimit=Inf)
    plan_rc! = plan_fft!(u;  flags=FFTW.MEASURE, timelimit=Inf)
    plan_cr! = plan_ifft!(u; flags=FFTW.MEASURE, timelimit=Inf)
    return plan_rc, plan_cr, plan_rc!, plan_cr!
end

function random_forcing!(f, fx, fy, kmin, kmax, Amp_forcing)
    """ 
    Generates a random forcing field in Fourier space for the 2D Navier-Stokes equations. It
    takes in the following input arguments:

        fx: a 2D array representing the x-component of the forcing field.
        fy: a 2D array representing the y-component of the forcing field.
        kmin: the minimum wavenumber for the forcing.
        kmax: the maximum wavenumber for the forcing.
        Amp_forcing: the amplitude of the forcing.
    """

    fx .= 0.0 + 0.0*im
    fy .= 0.0 + 0.0*im
    krange = (kmin^2 .<= k2 .<= kmax^2)
    phases = (2π) .* rand(sum(krange))
    norms = 1 ./ kk[krange]
    fx[krange] .= norms .* (cos.(phases) .+ 1im .* sin.(phases))
    phases = (2π) .* rand(sum(krange))
    fy[krange] .= norms .* (cos.(phases) .+ 1im .* sin.(phases))
    # energy = 0.5*mean(abs.(fx).^2 .+ abs.(fy).^2)
    # fx .*= Amp_forcing / energy
    # fy .*= Amp_forcing / energy

    # Take rotor to make it incompressible
    rotor!(f, fx, fy)
    energy = 0.5*mean(abs.(f).^2) 
    f .*= (Amp_forcing / energy)

    nothing
end

function gaussian_forcing!(f, kcenter, width, Amp_forcing)
    """ 
    Generates a gaussian forcing field in Fourier space for the 2D Navier-Stokes equations. It
    takes in the following input arguments:

        fx: a 2D array representing the x-component of the forcing field.
        fy: a 2D array representing the y-component of the forcing field.
        kcenter: wave number where the gaussian is centered.
        width: width of the gaussian.
        Amp_forcing: the amplitude of the forcing.
    """
    C = Amp_forcing/dx/dy / (2π*width^2)
    # C = Amp_forcing #/ (2π*width^2)
    # krange = (kmin^2 .<= k2 .<= kmax^2)
    @inbounds for j=1:ny
        # kyj = ky[j]
        @inbounds for i=1:nx
            k = kk[i,j]
            phase = 2π*rand()
            if abs(k - kcenter) < 2*width
                f[i,j] = C*exp(-0.5*((k - kcenter)/width)^2) * (cos(phase) + 1im*sin(phase))
            else
                f[i,j] = 0.0 + 0.0im
            end
        end
    end
end

function save_fields(ψ, index)
    """
    Save velocity fields obtained from stream function in files of the type
        vx.053.txt
        vy.100.txt
        psi.000.txt
    """
    output_dir = "outs/"
    filename1 = output_dir*"vx."*lpad(index,3,"0")*".txt"
    filename2 = output_dir*"vy."*lpad(index,3,"0")*".txt"
    filename3 = output_dir*"psi."*lpad(index,3,"0")*".txt"
    u, v = velocity_from_streamfunction(ψ)
    u_out = real.(plan_cr*u)
    v_out = real.(plan_cr*v)
    ψ_out = real.(plan_cr*ψ)
    writedlm(filename1, u_out)
    writedlm(filename2, v_out)
    writedlm(filename3, ψ_out)
    nothing
end

function read_streamfunction(path, index)
    filename = path*"psi."*lpad(index,3,"0")*".txt"
    ψ = readdlm(filename)
    return ψ
end

function solve_navier_stokes(ψ0::Array{T,2}, ν, dt, nsteps, f, fx, fy, to) where {T}
    """
    Main Navier-Stokes solver in two-dimensions using a pseudospectral method and periodic boundary conditions. It implements a Runge-Kutta method of order 4 for the time evolution.

    Inputs:
    ψ0: Initial stream function in real space.
    ν: Viscosity.
    dt: Time step.
    nsteps: Number of steps in the evolution.
    f: External forcing in Fourier space.
    to: Timer output.

    Outputs:
    ψ: stream function in Fourier space.
    """

    # Define the initial condition

    ψ = plan_rc*ψ0
    ω = similar(ψ)
    J = similar(ψ)
    k1 = similar(ψ)
    k2 = similar(ψ)
    k3 = similar(ψ)
    k4 = similar(ψ)

    # Save initial condition
    if restart==0
        @timeit to "save" save_fields(ψ, restart)
    end

    # Define the time step function using Euler and modifies in place ψ
    function step_function!(ψ::Array{T,2}, ω::Array{T,2}, J::Array{T,2}, k1::Array{T,2}, 
        ν, dt, f::Array{T,2}, fx::Array{T,2}, fy::Array{T,2}) where {T}
        """
        Time evolution using RK4.
        """

        # Compute the right-hand side of the Navier-Stokes equations in k1
        @timeit to "RHS" navier_stokes_rhs!(k1, ψ, ω, J, ν, f, fx, fy; to)
        ψ .+= dt*k1

        nothing
    end

    # Define the time step function using RK2 and modifies in place ψ
    function step_function!(ψ::Array{T,2}, ω::Array{T,2}, J::Array{T,2}, k1::Array{T,2}, 
        k2::Array{T,2}, ν, dt, f::Array{T,2}, fx::Array{T,2}, 
        fy::Array{T,2}) where {T}
        """
        Time evolution using RK4.
        """

        # Compute the right-hand side of the Navier-Stokes equations
        @timeit to "RHS" navier_stokes_rhs!(k1, ψ, ω, J, ν, f, fx, fy; to)

        # Compute the intermediate solutions for RK4
        ψint = ψ .+ (dt).*k1
        @timeit to "RHS" navier_stokes_rhs!(k2, ψint, ω, J, ν, f, fx, fy; to)

        # Compute the final solution
        ψ .+= (dt/2) .* (k1 .+ k2)

        nothing
    end

    # Define the time step function using RK4 and modifies in place ψ
    function step_function!(ψ::Array{T,2}, ω::Array{T,2}, J::Array{T,2}, k1::Array{T,2}, 
        k2::Array{T,2}, k3::Array{T,2}, k4::Array{T,2}, ν, dt, f::Array{T,2}, fx::Array{T,2}, 
        fy::Array{T,2}) where {T}
        """
        Time evolution using RK4.
        """

        # Compute the right-hand side of the Navier-Stokes equations
        @timeit to "RHS" navier_stokes_rhs!(k1, ψ, ω, J, ν, f, fx, fy; to)

        # Compute the intermediate solutions for RK4
        ψint = ψ .+ (0.5*dt).*k1
        @timeit to "RHS" navier_stokes_rhs!(k2, ψint, ω, J, ν, f, fx, fy; to)
        ψint .= ψ .+ (0.5*dt).*k2
        @timeit to "RHS" navier_stokes_rhs!(k3, ψint, ω, J, ν, f, fx, fy; to)
        ψint .= ψ .+ dt.*k3
        @timeit to "RHS" navier_stokes_rhs!(k4, ψint, ω, J, ν, f, fx, fy; to)

        # Compute the final solution
        ψ .+= (dt/6) .* (k1 .+ 2 .* k2 .+ 2 .* k3 .+ k4)

        nothing
    end

    # Define the time loop
    save_idx = restart+1
    @showprogress "Simulation ..." for i=1:nsteps 
        # Evolve ψ in time
        # @timeit to "Euler" step_function!(ψ, ω, J, k1, ν, dt, f, fx, fy)
        @timeit to "RK2" step_function!(ψ, ω, J, k1, k2, ν, dt, f, fx, fy)
        # @timeit to "RK4" step_function!(ψ, ω, J, k1, k2, k3, k4, ν, dt, f, fx, fy)

        if mod(i,nsave)==0
            # Check it's running well
            if sum(isnan.(ψ)) > 0
                throw(error("Diverge in step $i"))
            end

            @timeit to "save" save_fields(ψ, save_idx)
            save_idx += 1
        end

    end

    return ψ
end

####

# Main code starts here

# Parameters
nx = 64          # Number of grid points in x
ny = 64          # Number of grid points in y
Lx = 2π         # Length in x
Ly = 2π         # Length in y
ν  = 0.01       # Viscosity
α  = 0.001       # Large scale dissipation
dt = 0.001        # Temporal time step
nsteps = 20000    # Total number of time steps for the evolution
nsave = 100       # Saving each nsave time steps
restart = 0

# Initial conditions
Amp = 1. # Ampltitud of perturbation
E = .5     # Energy of initial condition

# Forcing
Amp_forcing = 1.   # Amplitud of the forcing
kmin_forcing = 1  # Minimum wave number of the forcing
kmax_forcing = 2  # Maximum wave number of the forcing
kcenter = 13
kwidth = 2

dx = Lx/nx       # Spatial resolution in x
dy = Ly/ny       # Spatial resolution in y
t = Array(0:dt:dt*nsteps) # Time

# Spectral quantities
ψ = zeros(ComplexF64, nx, ny)
plan_rc, plan_cr, plan_rc!, plan_cr! = init_fft(ψ)
kx, ky, kk, k2, Kill = init_spectral(nx, ny, dx, dy)

# Define grid for initial condition
x = range(0, Lx, length=nx+1)[1:nx]  # Grid points in x-direction
y = range(0, Ly, length=ny+1)[1:ny]  # Grid points in y-direction
XX, YY = meshgrid(x, y)

# Define external forcing
f = zeros(ComplexF64, nx, ny)
fx = zeros(ComplexF64, nx, ny)
fy = zeros(ComplexF64, nx, ny)
# random_forcing!(f, fx, fy, kmin_forcing, kmax_forcing, Amp)
# gaussian_forcing!(fx, fy, kcenter, kwidth, Amp_forcing)

# Define the initial condition in real space
if restart == 0
    ψ0 =  sin.(1 .* XX) .* sin.(2 .* YY) .+ randn(Random.seed!(123), nx,ny).*(Amp*dx)
    # ψ0 =  sin.(XX) .+ randn(Random.seed!(123), nx,ny).*(Amp*dx)
    # ψ0 = zeros(nx, ny)
    # random_forcing!(ψ, fx, fy, kmin_forcing, kmax_forcing, Amp)
    # ψ0 = real(plan_cr*ψ)

    # Compute velocity field in real space
    ψ0f = plan_rc*ψ0
    u0f, v0f = velocity_from_streamfunction(ψ0f)
    u0 = real(plan_cr*u0f)
    v0 = real(plan_cr*v0f)

    # Normalize energy of initial condition
    energy = 0.5*mean(u0.^2 .+ v0.^2) 
    println(energy)
    if energy!=0
        ψ0 .*= E/energy
    end

else
    ψ0 = read_streamfunction("outs/", restart)
end

# f .= 0.0 + 0.0*im
# gaussian_forcing!(f, kcenter, kwidth, Amp_forcing)

# Solve
to = TimerOutput()
ψ = @timeit to "main" solve_navier_stokes(ψ0, ν, dt, nsteps, f, fx, fy, to) 

# ω = similar(ψ)
# uf, vf = velocity_from_streamfunction(ψ)
# rotor!(ω, uf, vf)
# u = real.(plan_cr*uf)
# v = real.(plan_cr*vf)

print_timer(to)

