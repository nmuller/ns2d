import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import sys

path = "./outs/"
nfiles = 100

if len(sys.argv)>1:
    path0 = sys.argv[1]
if len(sys.argv)>2:
    nfiles = int(sys.argv[2])

vmin = -1.
vmax = 1.
cmap = plt.cm.coolwarm

def load_velocity(path,index):
  vx=np.loadtxt(path+"vx."+str(index).zfill(3)+".txt")
  vy=np.loadtxt(path+"vy."+str(index).zfill(3)+".txt")
  return vx, vy

def compute_vorticity(vx, vy):
    return -(np.gradient(vy, axis=0) - np.gradient(vx, axis=1))

def compute_divergence(vx, vy):
    return (np.gradient(vx, axis=0) + np.gradient(vy, axis=1))

def run_animation():
    anim_running = True

    # Function to allow pause/play with click
    def onClick(event):
        nonlocal anim_running
        if anim_running:
            anim.event_source.stop()
            anim_running = False
        else:
            anim.event_source.start()
            anim_running = True

    def animFunc(frame):
        vx, vy = load_velocity(path, frame)
        vort = compute_vorticity(vx, vy)
        im.set_array(vort)
        ax.set_title(frame)
        return im,

    fig.canvas.mpl_connect('button_press_event', onClick)

    anim = animation.FuncAnimation(fig, animFunc, interval=50, frames=nfiles+1,
            blit=False, repeat_delay=1000, repeat=True)

vx, vy = load_velocity(path,0) 
NPx, NPy = vx.shape
fig = plt.figure(1)
ax = plt.axes(xlim=(0, NPx), ylim=(0, NPy))
print(vx.shape)
vort = compute_vorticity(vx, vy)
div = compute_divergence(vx, vy)
print(np.sum(div))
im = plt.imshow(vort, animated=False,vmin=vmin,vmax=vmax,cmap=cmap)
plt.title(0)
plt.colorbar()

run_animation()

#ani = animation.FuncAnimation(fig, run_animation, interval=50, blit=True,
#                                repeat_delay=1000,repeat=True)

#ani.save('dipolar_supersolid.gif', writer='imagemagick')

plt.draw()
plt.show()
